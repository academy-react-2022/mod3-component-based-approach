import React, { lazy, Suspense } from 'react';
import './App.css';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { NavBar } from './core/NavBar';
import { NewsPage } from './pages/cms/news/NewsPage';
import { ProductPage } from './pages/cms/products/ProductPage';
const HomePage = lazy(() => import('./pages/home/HomePage'));
const CMSPage = lazy(() => import('./pages/cms/CMSPage'));

function App() {
  return (
    <BrowserRouter>
      <NavBar />
      <MyRoutes />
    </BrowserRouter>
  );
}

export default App;


///

export function MyRoutes() {
  return <Routes>
    <Route path="home" element={
      <Suspense fallback={<div>loading...</div>}>
        <HomePage />
      </Suspense>
    }/>

    <Route path="cms" element={
      <Suspense fallback={<div>loading...</div>}>
        <CMSPage />
      </Suspense>
    }>
      <Route index element={ <Navigate to="news" />} />
      <Route path="products" element={<ProductPage />} />
      <Route path="news" element={<NewsPage />} />
    </Route>

    <Route index element={ <Navigate to="cms" /> } />
  </Routes>
}



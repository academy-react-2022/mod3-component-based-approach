import axios from 'axios';
import { useEffect, useState } from 'react';
import { Item, PartialItem } from '../../../../model/item';

const initialState: Item[] = []

export function useProducts() {
  const [list, setList] = useState<PartialItem[]>(initialState);
  const [showModal, setShowModal] = useState<boolean>(false);
  const [selectedItem, setSelectedItem] = useState<PartialItem | null>(null);

  useEffect(() => {
    axios.get<PartialItem[]>(`${process.env.REACT_APP_BASEURL}/products`)
      .then(res => setList(res.data))
  }, [])

  function deleteItem(id: number) {
    axios.delete(`http://localhost:3001/products/${id}`)
      .then(() => {
        setList(s => s.filter(item => item.id !== id))
      })
  }

  function showModalForEdit(item: PartialItem) {
    setSelectedItem(item);
    setShowModal(true);
  }

  function editItem(formData: Partial<Item>) {
    axios.patch<PartialItem>(`http://localhost:3001/products/${formData.id}`, formData)
      .then(res => setList(s => s.map(item => {
        return item.id === formData.id ? res.data : item;
      })))
  }

  function addItem(formData: Partial<Item>) {
    axios.post('http://localhost:3001/products', formData)
      .then(res => setList(s => [...s, res.data]))
  }

  function saveItem(formData: Partial<Item>) {
    console.log(formData)
    if (formData.id) {
      editItem(formData)
    } else {
      addItem(formData)
    }

    setShowModal(false)
    setSelectedItem(null);
  }

  function openModal() {
    setShowModal(true)
  }

  return {
    list,
    selectedItem,
    showModal,
    actions: {
      deleteItem,
      openModal,
      showModalForEdit,
      saveItem,
    }
  };
}

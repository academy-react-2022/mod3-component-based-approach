import { ProductsForm } from './components/ProductsForm';
import { ProductList } from './components/ProductsList';
import { useProducts } from './hooks/useProducts';

export function ProductPage() {
  const { list, selectedItem, showModal, actions } = useProducts();

  return <div>
    <Alert total={list.length} label="products" type="success" />
    <i className="fa fa-plus-circle fa-2x" onClick={actions.openModal}></i>

    {
      showModal && <ProductsForm onSave={actions.saveItem} item={selectedItem}/>
    }

    <ProductList data={list} onItemRemove={actions.deleteItem} onItemEdit={actions.showModalForEdit}/>
  </div>
}

// =======
interface AlertProps {
  total: number;
  label?: string;
  type?: 'success' | 'danger' | 'info';
}
const Alert = (props: AlertProps) => {
  const { total, label = 'items', type = 'info'} = props;

  return  <div className={`alert alert-${type}`}>
   {total} {label}
  </div>
}
export default Alert;

import { useState } from 'react';
import { PartialItem } from '../../../../model/item';


interface ProductsFormProps {
  item: PartialItem | null;
  onSave: (formData: PartialItem) => void;
}

const initialState = { name: '', cost: 0};

export function ProductsForm (props: ProductsFormProps) {
  const [formData, setFormData] = useState<PartialItem>(props.item || initialState);

  function changeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    setFormData(s => ({
      ...s,
      [e.target.name]: e.target.value
    }))
  }

  function submitHandler(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    props.onSave(formData)
  }

  return (
    <div style={{
      position: 'fixed', top: 0, right: 0, bottom: 0, left: 0, background: 'white',
      zIndex: 100
    }}>
      <form onSubmit={submitHandler}>
        <input
          type="text" value={formData.name}
          name="name"
          onChange={changeHandler}/>

        <input
          type="text" value={formData.cost}
          name="cost"
          onChange={changeHandler}/>

        {JSON.stringify(formData)}
        <button type="submit">
          {formData.id ? 'EDIT' : 'ADD'}
        </button>
      </form>
    </div>
  )
}

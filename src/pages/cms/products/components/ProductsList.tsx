import { PartialItem } from '../../../../model/item';

interface ProductListProps {
  data: PartialItem[];
  onItemRemove: (id: number) => void;
  onItemEdit: (item: PartialItem) => void;
}
export function ProductList(props: ProductListProps) {
  console.log('render list')
  return <ul>
    {
      props.data.map((item) => {
        return <li key={item.id}>
          {item.name}
          <i
            className="fa fa-trash"
            onClick={() => {
              if (item.id)
                props.onItemRemove(item.id)
            }}
          />

            <i
              className="fa fa-edit"
              onClick={() => props.onItemEdit(item)}
            />
          </li>
        })
    }
  </ul>
}


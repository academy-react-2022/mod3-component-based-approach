import { NavLink, Outlet } from 'react-router-dom';

export default function CMSPage() {
  return <div>
    <h1>CMS</h1>
    <NavLink to="news" className="btn btn-outline-dark">News</NavLink>
    <NavLink to="products" className="btn btn-outline-dark">Products</NavLink>

    <Outlet />

  </div>
}

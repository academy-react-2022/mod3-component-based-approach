import { NavLink } from 'react-router-dom';

export function NavBar() {
  return <div className="bg-dark p-4 w-100">
    <div className="btn-group">
      <NavLink
        to="/home"
        className="btn btn-outline-info"
      >
        Home
      </NavLink>

      <NavLink
        to="/cms"
        className="btn btn-outline-info"
      >
        Cms
      </NavLink>
    </div>
  </div>
}

export interface Item {
  id: number,
  name: string;
  cost: number;
}

export type PartialItem = Partial<Item>;

